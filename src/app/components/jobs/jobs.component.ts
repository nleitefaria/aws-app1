import { Component, OnInit } from '@angular/core';
import { JobsService } from  '../../services/jobs.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

    location = 'Jobs';
    jobs:  Array<object> = [];
    jobsPage:  Array<object> = [];
    p: number = 1;
    total: number;
    loading: boolean;

    constructor(private jobsService:  JobsService) { }
    
    ngOnInit() 
    {
        this.getPage(1);
    }
    
    getPage(page: number)
    {
        this.loading = true;  
        this.jobsService.getJobsPage(page).subscribe(
            response =>{
                if(response.error) { 
                    alert('Server Error');
                } else {                                                
                    this.p = page;                                                              
                    this.total = response.totalElements;
                    this.jobsPage = response.content; 
                    this.loading = false;  
                    console.log(response);                          
                }
            },
            error =>{
                alert('Server error');
            }
        );
    }

}
