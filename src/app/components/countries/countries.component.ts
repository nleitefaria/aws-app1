import { Component, OnInit } from '@angular/core';
import { CountriesService } from  '../../services/countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit 
{
  location = 'Countries';
  countries:  Array<object> = [];
  countriesPage:  Array<object> = [];
  p: number = 1;
  total: number;
  loading: boolean;

  constructor(private  countriesService:  CountriesService) { }

  ngOnInit() 
  {
      this.getPage(1);
  }
  
  getPage(page: number)
  {
      this.loading = true;  
      this.countriesService.getCountriesPage(page).subscribe(
          response =>{
              if(response.error) { 
                  alert('Server Error');
              } else {                                                
                  this.p = page;                                                              
                  this.total = response.totalElements;
                  this.countriesPage = response.content; 
                  this.loading = false;  
                  console.log(response);                          
              }
          },
          error =>{
              alert('Server error');
          }
      );
  }
}
