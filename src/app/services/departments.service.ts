import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

    private BASE_URL:string = environment.apiUrl;

    constructor(private  httpClient:  HttpClient) 
    {      
    }

    getDepartments(){
        return  this.httpClient.get(`${this.BASE_URL}/department/list`);
    }

    getDepartmentsPage(page: number):any
    {    
        return  this.httpClient.get(`${this.BASE_URL}/department/list/${page}`);
    }
}