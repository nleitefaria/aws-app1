import { Component, OnInit } from '@angular/core';
import { DepartmentsService } from  '../../services/departments.service';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit 
{
    location = 'Departments';
    departments:  Array<object> = [];
    departmentsPage:  Array<object> = [];
    p: number = 1;
    total: number;
    loading: boolean;

    constructor(private departmentsService:  DepartmentsService) { }
    
    ngOnInit() 
    {
        this.getPage(1);
    }
    
    getPage(page: number)
    {
        this.loading = true;  
        this.departmentsService.getDepartmentsPage(page).subscribe(
            response =>{
                if(response.error) { 
                    alert('Server Error');
                } else {                                                
                    this.p = page;                                                              
                    this.total = response.totalElements;
                    this.departmentsPage = response.content; 
                    this.loading = false;  
                    console.log(response);                          
                }
            },
            error =>{
                alert('Server error');
            }
        );
    }

}
