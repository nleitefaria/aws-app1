import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

    private BASE_URL:string = environment.apiUrl;

    constructor(private  httpClient:  HttpClient) 
    {      
    }

    getJobs()
    {
        return this.httpClient.get(`${this.BASE_URL}/job/list`);
    }

    getJobsPage(page: number):any
    {    
        return this.httpClient.get(`${this.BASE_URL}/job/list/${page}`);
    }
}