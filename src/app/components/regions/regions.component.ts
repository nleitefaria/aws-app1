import { Component, OnInit } from '@angular/core';
import { RegionsService } from  '../../services/regions.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class RegionsComponent implements OnInit 
{
    location = 'Regions';
    regions:  Array<object> = [];
    regionsPage:  Array<object> = [];
    p: number = 1;
    total: number;
    loading: boolean;

    constructor(private regionsService:  RegionsService) { }
    
    ngOnInit() 
    {
        this.getPage(1);
    }
    
    getPage(page: number)
    {
        this.loading = true;  
        this.regionsService.getRegionsPage(page).subscribe(
            response =>{
                if(response.error) { 
                    alert('Server Error');
                } else {                                                
                    this.p = page;                                                              
                    this.total = response.totalElements;
                    this.regionsPage = response.content; 
                    this.loading = false;  
                    console.log(response);                          
                }
            },
            error =>{
                alert('Server error');
            }
        );
    }

}
