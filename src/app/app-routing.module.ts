import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CountriesComponent } from './components/countries/countries.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { LocationsComponent } from './components/locations/locations.component';
import { RegionsComponent } from './components/regions/regions.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},   
    { 
        path: '', component: HomeComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'countries', component: CountriesComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'departments', component: DepartmentsComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'employees', component: EmployeesComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'jobs', component: JobsComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'locations', component: LocationsComponent, 
        pathMatch: 'full'
    },
    { 
        path: 'regions', component: RegionsComponent, 
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }