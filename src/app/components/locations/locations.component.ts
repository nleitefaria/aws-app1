import { Component, OnInit } from '@angular/core';
import { LocationsService } from  '../../services/locations.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit 
{
    location = 'Locations';
    locations:  Array<object> = [];
    locationsPage:  Array<object> = [];
    p: number = 1;
    total: number;
    loading: boolean;

    constructor(private locationsService:  LocationsService) { }
    
    ngOnInit() 
    {
        this.getPage(1);
    }
    
    getPage(page: number)
    {
        this.loading = true;  
        this.locationsService.getLocationsPage(page).subscribe(
            response =>{
                if(response.error) { 
                    alert('Server Error');
                } else {                                                
                    this.p = page;                                                              
                    this.total = response.totalElements;
                    this.locationsPage = response.content; 
                    this.loading = false;  
                    console.log(response);                          
                }
            },
            error =>{
                alert('Server error');
            }
        );
    }
 
}
