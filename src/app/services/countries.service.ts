import { Injectable } from  '@angular/core';
import { HttpClient} from  '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
providedIn:  'root'
})
export class CountriesService 
{
    private BASE_URL:string = environment.apiUrl;

    constructor(private  httpClient:  HttpClient) 
    {      
    }
    
    getCountries(){
        return  this.httpClient.get(`${this.BASE_URL}/country/list`);
    }
    
    getCountriesPage(page: number):any
    {    
        return  this.httpClient.get(`${this.BASE_URL}/country/list/${page}`);
    }
    
    
}
