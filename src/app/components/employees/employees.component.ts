import { Component, OnInit } from '@angular/core';
import { EmployeesService } from  '../../services/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit 
{
    location = 'Employees';
    employees:  Array<object> = [];
    employeesPage:  Array<object> = [];
    p: number = 1;
    total: number;
    loading: boolean;

    constructor(private employeesService:  EmployeesService) { }
    
    ngOnInit() 
    {
        this.getPage(1);
    }
    
    getPage(page: number)
    {
        this.loading = true;  
        this.employeesService.getEmployeesPage(page).subscribe(
            response =>{
                if(response.error) { 
                    alert('Server Error');
                } else {                                                
                    this.p = page;                                                              
                    this.total = response.totalElements;
                    this.employeesPage = response.content; 
                    this.loading = false;  
                    console.log(response);                          
                }
            },
            error =>{
                alert('Server error');
            }
        );
    }
}