import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {

    private BASE_URL:string = environment.apiUrl;

    constructor(private  httpClient:  HttpClient) 
    {      
    }

    getLocations()
    {
        return this.httpClient.get(`${this.BASE_URL}/location/list`);
    }

    getLocationsPage(page: number):any
    {    
        return this.httpClient.get(`${this.BASE_URL}/location/list/${page}`);
    }
}